from datetime import datetime


def handle(req: str) -> str:
    """handle a request to the function
    Args:
        req (str): request body
    """

    return f"Hello {req}, openfaas is the best at {datetime.today()}"
