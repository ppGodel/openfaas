from datetime import date


def handle(req: str) -> str:
    """handle a request to the function
    Args:
        req (str): request body
    """

    return f"Hello {req}, {date.today()}"
