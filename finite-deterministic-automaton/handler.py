from functools import reduce
from typing import Callable, Iterable, Dict, Set, Tuple, Any
import json


def compose_functions(f: Callable, g: Callable) -> Callable:
    def fog(x: Any) -> Any:
        return g(f(x))

    return fog


def or_function(v1: bool, v2: bool) -> bool:
    return v1 or v2


def deterministic_automate(
    sigma: Set[chr], delta: Dict[Tuple[chr, str], str], F: Set[str], s: str
):
    def delta_fn(state: str, char: chr) -> str:
        print(f'{state} , {char} : {delta.get((state,char), "_")}')
        return delta.get((state, char), "_")

    def partial_delta(char: chr) -> Callable[[str], str]:
        def delta_eval(state: str) -> str:
            return delta_fn(state, char)

        return delta_eval

    # d(q,s) -> q_1 => d(q_1, s_1) ::  p(s, d) -> dp
    # :: dp(q) -> q_1 => dp(q_1) => ...
    # f(x) -> y => g(y) -> z : fog(x) -> z
    def compose_delta_transitions(functions: Iterable[Callable]) -> Callable:
        return reduce(compose_functions, functions)

    def create_delta_transitions(word: str) -> Iterable[Callable[[str], str]]:
        return map(partial_delta, word) if len(word) > 0 else (lambda x: x, lambda x: x)

    def evaluate(word: str) -> bool:
        return compose_delta_transitions(create_delta_transitions(word))(s) in F

    if reduce(or_function, (v not in sigma for k, v in delta.keys())):
        raise Exception("char in delta is not in sigma")
    return evaluate


def str_to_delta(delta_str: str) -> Dict:
    def split_into_tuple(str_entry: str) -> Tuple[str, str]:
        s_q = str_entry.replace(" ", "").replace("(", "").replace(")", "").split(",")
        if len(s_q) != 2:
            raise Exception(f"missing tuple in {str_entry}, {s_q}")
        return (s_q[0], s_q[1])

    delta_raw = json.loads(delta_str)  # type: Dict[str,str]
    return {split_into_tuple(k): v for k, v in delta_raw.items()}

    return {}


def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """
    required_values = ["delta", "sigma", "F", "s", "w"]
    try:
        fda_init = json.loads(req)  # type: Dict
    except json.JSONDecodeError as jde:
        response = f"Invalid json, {jde}"
        return response
    missing_values = [
        required_value
        for required_value in required_values
        if required_value not in fda_init.keys()
    ]
    if any(missing_values):
        response = f'missing parameters {",".join(missing_values)} in request'
        return response
    _F = set(fda_init["F"])
    _sigma = set(fda_init["sigma"])
    _delta = str_to_delta(fda_init["delta"])

    da = deterministic_automate(
        sigma=_sigma,
        delta=_delta,
        F=_F,
        s=fda_init["s"],
    )
    response = da(fda_init["w"])
    return response
